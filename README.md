# Sensus API

This is the documentation and sample repository for the Sensus BPM Online API.

## Key features

Our API is based on GraphQL and allows you to, among others:

- Retrieve projects and project versions
- Retrieve all types of Sensus elements from the projects
- Retrieve all template data
- Add custom category items and groups
- Add template information for basic template fields

## Getting access

To get access to our API, please contact contact us at info@sensus-processmanagement.com. API acccess must be part of your contract in order to access it.

Accessing the API works via [OAuth2.0 Authorization Grant Flow](https://oauth.net/2/grant-types/authorization-code/). You will be provided with:

- a client id (`CLIENT_ID`)
- a client secret (`CLIENT_SECRET`)
- authorization url (`AUTH_URL`)
- API url (`API_URL`)

These are standard for the OAuth2.0 flow. Besides that we will provide you with a token (TOKEN) which we use for some additional identification purposes in our backend.

Using the `CLIENT_ID`, `CLIENT_SECRET` and `AUTH_URL` it is possible to retrieve an `ACCESS_TOKEN` via the OAuth2.0 flow. Based on the `ACCESS_TOKEN` the Sensus backend knows which license is accessed and which projects belong to that license.

## GraphQL

[GraphQL](https://graphql.org/) is used since it provides a lot of benefits over typical REST API's for our use-case:

- The Sensus data model is very hierarchical with a lot of cross cutting links, which makes a default JSON / XML api difficult.
- GraphQL allows the API-consumer to specifically fetch the fields needed from the model
- It is strictly type based and as such it is self-documenting. By using proper tooling (like [GraphQL Playground](https://github.com/prisma-labs/graphql-playground)) it is possible to introspect the schema and navigate through the type hierarchy.

It is advised to read the documentation of [GraphQL](https://graphql.org/) in order to fully understand _how_ to use it.

## Querying the API

### POST Request

All the API requests are done via a POST request to the API_URL. The following headers need to be present:

- `token: TOKEN`
- `Authorization: Bearer ACCESS_TOKEN`
- `Content-Type: application/json`
- `Accept: application/json`

A POST request is then performed with the following request body:

```json
{
  "query": "QUERY",
  "operationName": "OPERATION_NAME", //optional
  "variables": "VARIABLES" // optional
}
```

Where `QUERY` is a JSON-escaped string of the GraphQL query. `OPERATION_NAME` is optional a different operation name is used. `VARIABLES` is optional and is a JSON structure with variables to be used. Please refer to the documentation of GraphQL to understand exactly how this request works.

### Guids

All elements within the Sensus data model are referred to with [GUIDs (or UUIDs)](https://en.wikipedia.org/wiki/Universally_unique_identifier). This guarantees an almost unique identifier for every element in our model so it will appear often in GraphQL requests and responses.

### First query

To check if everything works, the following query can be run to retrieve the licenses currently available to you:

```json
{
  "query": "{ licenses { guid, name } }"
}
```

This will result in a similar response:

```json
{
  "data": {
    "licenses": [
      {
        "guid": "00000000-ffff-aaaa-bbbb-cccccccccccc", // a GUID of your license
        "name": "Your license name"
      }
    ]
  }
}
```

If you get a similar response, everything works correctly.

### Example queries

Here are some example queries to get you started. Please note that we are only showing the GraphQL query and not the escaped version to be used in the POST request.

#### Get all projects and their guids

_Query_

```graphql
query Example($licenseGuid: UUID!) {
  licenseByGuid(guid: $licenseGuid) {
    projects {
      guid
      name
    }
  }
}
```

_Variables_

```json
{
  "licenseGuid": "your license guid"
}
```

#### Get a specific project and the version data

_Query_

```graphql
query Example($licenseGuid: UUID!, $projectGuid: UUID!) {
  project(licenseGuid: $licenseGuid, projectGuid: $projectGuid) {
    name
    versions {
      guid
      state
      finalizeDate
    }
  }
}
```

_Variables_

```json
{
  "licenseGuid": "your license guid",
  "projectGuid": "a project guid"
}
```

#### Get the a version of a project and get the full process tree of all processes

_Query_

```graphql
query Example(
  $licenseGuid: UUID!
  $projectGuid: UUID!
  $projectVersionGuid: UUID!
) {
  projectVersion(
    licenseGuid: $licenseGuid
    projectGuid: $projectGuid
    projectVersionGuid: $projectVersionGuid
  ) {
    processes {
      organisations {
        guid
        totalSequence
        name
        themes {
          guid
          totalSequence
          name
          mainProcesses {
            guid
            totalSequence
            name
            processes {
              guid
              totalSequence
              name
              activities {
                guid
                totalSequence
                name
              }
            }
          }
        }
      }
    }
  }
}
```

_Variables_

```json
{
  "licenseGuid": "your license guid",
  "projectGuid": "a project guid",
  "projectVersionGuid": "a projectversion guid"
}
```

## Further information

The full schema is available via introspection using a GraphQL tool like GraphQL Playground. The current schema in GraphQL `SDL` is available in the `schema` folder. This is the full type hierarchy of the GraphQL API.

The schema's are split into different files for the different domains of the model. The starting point is `server.graphql`.

In the `examples` folder examples will be placed for more complex queries and applications.

## Assistance

Please note that any assistance in integrating the Sensus API in your own applications is not part of the contract with Sensus. If involvement is needed from Sensus to use the API, please contact us to assess the needs and possible costs involved.
