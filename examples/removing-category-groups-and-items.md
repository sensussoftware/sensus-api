# Removing category groups and items

This chapter follows the [Adding Category Groups and Items chapter](./adding-category-groups-and-items.md).

Just like adding, it is also possible to remove Category Groups and Items. We use the same category as in the chapter mentioned above.

The starting point is:

![picture](../img/applications_3.svg)

And the goal is to remove the default Application group.

This structure can be found via GraphQL like follows:

_Query_

```
query Example($licenseGuid: UUID!, $projectGuid: UUID!,$projectVersionGuid: UUID!, $categoryGuid: UUID!) {
  projectVersion(licenseGuid: $licenseGuid, projectGuid: $projectGuid, projectVersionGuid: $projectVersionGuid) {
    categoriesByGuid(guids: [$categoryGuid]) {
      groups {
        guid
        name
        items {
          guid
          name
        }
      }
    }
  }
}
```

_Response_

```json
{
  "data": {
    "projectVersion": {
      "categoriesByGuid": [
        {
          "groups": [
            {
              "guid": "f5e597ad-512e-cf93-930f-1a316fd0f028",
              "name": "Application group",
              "items": [
                {
                  "guid": "13c6ba38-9c29-4b58-e276-0df3787fd182",
                  "name": "Application 1"
                },
                {
                  "guid": "b4cb5517-ea5f-382a-f669-99887ac1fe80",
                  "name": "Application 2"
                }
              ]
            },
            {
              "guid": "e01ed95a-e5e0-4f55-8e61-275aa0dd95b8",
              "name": "Office 365",
              "items": [
                {
                  "guid": "fbbc9fab-3c08-4b6e-9561-5894ec12a02f",
                  "name": "Word"
                },
                {
                  "guid": "b1dc87a4-0c06-4a4f-ac82-8a0b8b848c63",
                  "name": "Excel"
                },
                {
                  "guid": "9511885b-cc2c-4534-ad00-da742a528d0f",
                  "name": "Powerpoint"
                }
              ]
            }
          ]
        }
      ]
    }
  }
}
```

## Remove a category item

Let's remove `Application 1` (guid = `13c6ba38-9c29-4b58-e276-0df3787fd182`).

_Query_

```
mutation Example($licenseGuid: UUID!, $projectGuid: UUID!,$projectVersionGuid: UUID!) {
  projectVersion(licenseGuid: $licenseGuid, projectGuid: $projectGuid, projectVersionGuid: $projectVersionGuid) {
    removeCategoryItem(guid: "13c6ba38-9c29-4b58-e276-0df3787fd182")
  }
}
```

_Response_

```json
{
  "data": {
    "projectVersion": {
      "removeCategoryItem": true
    }
  }
}
```

Since we can not retrieve data from the removed item we get a boolean back if the removal is succesful.

If we look at the result this appears to be correct:

![picture](../img/applications_4.svg)

## Remove a category group

If we remove 'Application group' (`f5e597ad-512e-cf93-930f-1a316fd0f028`) we also remove the 'Application 2', since it is a child item.

_Query_

```
mutation Example($licenseGuid: UUID!, $projectGuid: UUID!,$projectVersionGuid: UUID!) {
  projectVersion(licenseGuid: $licenseGuid, projectGuid: $projectGuid, projectVersionGuid: $projectVersionGuid) {
    removeCategoryGroup(guid: "f5e597ad-512e-cf93-930f-1a316fd0f028")
  }
}
```

_Reponse_

```json
{
  "data": {
    "projectVersion": {
      "removeCategoryGroup": true
    }
  }
}
```

And the result is:

![picture](../img/applications_5.svg)
