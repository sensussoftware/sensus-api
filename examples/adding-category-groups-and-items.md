# Adding category groups and items

It is possible to mutate the data model, albeit in a limited form due to the increased complexity of adding elements.

Currently it is possible to add/remove the following `Icon` types:

- `CategoryGroup`
- `CategoryItem`

Category groups and items make up the following elements in Sensus:

- Applications
- Documents
- Roles
- Custom categories

Mutating the Departments and Functions (OrgChart) is planned to be added. Mutating the processes is not yet planned, since it is complex and error prone.

## Example: Adding an Application Group and Item

We have a default new project with the following Applications:

![picture](../img/applications_1.svg)

Let's add the application group defined as follows:

- Group: Office365
  - Item: Word
  - Item: Excel
  - Item: Powerpoint

In the schema file `project.graphql` the following type is defined:

```
type MutProjectVersion {
    addCategoryGroup(categoryGuid: UUID!, name: String!): CategoryGroup
    addCategorySubGroup(groupGuid: UUID!, name: String!): CategoryGroup
    addCategoryItem(groupGuid: UUID!, name: String!): CategoryItem

    removeCategoryGroup(guid: UUID!): Boolean
    removeCategoryItem(guid: UUID!): Boolean

    setTemplateValue(iconGuid: UUID!, templateGuid: UUID!, value: String!): TemplateValue
}
```

Since we need to add a group we need a `categoryGuid` first. Using that we can add a group and subsequently the items.

### Fetch the application category

First we need to query for the application category. We assume the license, project and projectVersion guids are known.

_Query_

```
query Example($licenseGuid: UUID!, $projectGuid: UUID!,$projectVersionGuid: UUID!) {
  projectVersion(licenseGuid: $licenseGuid, projectGuid: $projectGuid, projectVersionGuid: $projectVersionGuid) {
    categories {
      guid
      name
    }
  }
}
```

_Response_

```json
{
  "data": {
    "projectVersion": {
      "categories": [
        {
          "guid": null,
          "name": "Processes"
        },
        {
          "guid": null,
          "name": "Departments and positions"
        },
        {
          "guid": "ce9a8093-a3ac-5f73-b140-c8bbaa1741b0",
          "name": "Rolegroups and roles"
        },
        {
          "guid": "73712e86-2bf1-efb4-f678-cdabe832e067",
          "name": "Documents"
        },
        {
          "guid": "5dd4f703-4783-3851-be53-999bec234db1",
          "name": "Applications"
        },
        {
          "guid": "72e31c7e-058c-cdc1-30a2-1af77523dc2f",
          "name": "IT"
        }
      ]
    }
  }
}
```

As can be seen, the Applications category has guid `5dd4f703-4783-3851-be53-999bec234db1`.

If we query the Applications category we get the following:

_Query_

```
query Example($licenseGuid: UUID!, $projectGuid: UUID!,$projectVersionGuid: UUID!,$categoryGuid: UUID!) {
  projectVersion(licenseGuid: $licenseGuid, projectGuid: $projectGuid, projectVersionGuid: $projectVersionGuid) {
    categoriesByGuid(guids:[$categoryGuid]) {
      name
      groups {
        name
        items {
          name
        }
      }
    }
  }
}
```

_Response_

```json
{
  "data": {
    "projectVersion": {
      "categoriesByGuid": [
        {
          "name": "Applications",
          "groups": [
            {
              "name": "Application group",
              "items": [
                {
                  "name": "Application 1"
                },
                {
                  "name": "Application 2"
                }
              ]
            }
          ]
        }
      ]
    }
  }
}
```

This matches with the image above.

## Add a new group

We can add a new group by getting a Mutable version of the projectversion and executing the mutation:

_Query_

```
mutation Example($licenseGuid: UUID!, $projectGuid: UUID!,$projectVersionGuid: UUID!,$categoryGuid: UUID!) {
  projectVersion(licenseGuid: $licenseGuid, projectGuid: $projectGuid, projectVersionGuid: $projectVersionGuid) {
    addCategoryGroup(categoryGuid: $categoryGuid, name: "Office 365") {
      guid
      name
    }
  }
}
```

_Response_

```json
{
  "data": {
    "projectVersion": {
      "addCategoryGroup": {
        "guid": "e01ed95a-e5e0-4f55-8e61-275aa0dd95b8",
        "name": "Office 365"
      }
    }
  }
}
```

The newly added category group has guid `e01ed95a-e5e0-4f55-8e61-275aa0dd95b8`.
When inspecting the Designer we see that it indeed has been added.

![picture](../img/applications_2.svg)

### Add the items

The items can be added in one go, since we know everything now.

_Query_

```
mutation Example($licenseGuid: UUID!, $projectGuid: UUID!,$projectVersionGuid: UUID!, $groupGuid: UUID!) {
  projectVersion(licenseGuid: $licenseGuid, projectGuid: $projectGuid, projectVersionGuid: $projectVersionGuid) {
    word: addCategoryItem(groupGuid: $groupGuid, name: "Word") {
      guid
      name
    }

    excel: addCategoryItem(groupGuid: $groupGuid, name: "Excel") {
      guid
      name
    }

    powerpoint: addCategoryItem(groupGuid: $groupGuid, name: "Powerpoint") {
      guid
      name
    }
  }
}
```

_Response_

```json
{
  "data": {
    "projectVersion": {
      "word": {
        "guid": "fbbc9fab-3c08-4b6e-9561-5894ec12a02f",
        "name": "Word"
      },
      "excel": {
        "guid": "b1dc87a4-0c06-4a4f-ac82-8a0b8b848c63",
        "name": "Excel"
      },
      "powerpoint": {
        "guid": "9511885b-cc2c-4534-ad00-da742a528d0f",
        "name": "Powerpoint"
      }
    }
  }
}
```

Please note that we used a feature called aliasing of GraphQL in order to execute multiple mutations.

Inspecting the Designer we have the final result:

![picture](../img/applications_3.svg)
