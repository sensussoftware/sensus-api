# Setting template values

It is also possible to set template values of icons for a subset of the template fields:

- TEXT
- DATE
- TIME
- PERCENTAGE
- URL
- NUMBER

These are all simple scalar fields. In the future we might expand it with the more complex fields.

This chapter follows the [Adding Category Groups and Items chapter](./adding-category-groups-and-items.md).

## Getting the template

From the previous chapter we made an Office 365 group in the Applications category. We are not going to set some data of the Word application. Looking at the Word application in the Designer we see the following:

![picture](../img/word.png)

We are going to set the `Application` URL and the `Description`.

By looking at the GraphQL schema there is a mutation which is defined as:

```
type MutProjectVersion {
    // rest is omitted

    setTemplateValue(iconGuid: UUID!, templateGuid: UUID!, value: String!): TemplateValue
}
```

The `iconGuid` we already have from the previous chapter, whic is `fbbc9fab-3c08-4b6e-9561-5894ec12a02f`. By running the following query we fetch the templates:

_Query_

```
query GetTemplates($licenseGuid: UUID!, $projectGuid: UUID!, $projectVersionGuid: UUID!) {
  projectVersion(licenseGuid: $licenseGuid, projectGuid: $projectGuid, projectVersionGuid: $projectVersionGuid) {
    iconsByGuid(guids: ["fbbc9fab-3c08-4b6e-9561-5894ec12a02f"]) {
      guid
      name
      templates {
        guid
        name
       	systemType
        type
      }
    }
  }
}
```

And the response is:

```json
{
  "data": {
    "projectVersion": {
      "iconsByGuid": [
        {
          "guid": "fbbc9fab-3c08-4b6e-9561-5894ec12a02f",
          "name": "Word",
          "templates": [
            {
              "guid": "4e1825a0-142b-45d8-8efb-25eb17661c14",
              "name": "Description",
              "systemType": "Description",
              "type": "TEXT"
            },
            {
              "guid": "91cfb54b-22ee-4350-be74-a272830687cd",
              "name": "Application",
              "systemType": "ApplicationURL",
              "type": "URL"
            },
            {
              "guid": "7ba0b853-d036-416c-962f-b439bbcc266d",
              "name": "Application parameters",
              "systemType": "ApplicationParams",
              "type": "COMPOSED"
            }
          ]
        }
      ]
    }
  }
}
```

Here we see the following interesting items

- `guid`, which we need and is the `templateGuid`
- `name`, which speaks for itself
- `systemType`, which defines if it is a fieldtype that is always present and is added by the system, and if so, what type. These can be used in the `templateFilter` if needed.
- `type`, the actual type of the template

In this case we want to set the URL and the Description, so we need the following guids: `4e1825a0-142b-45d8-8efb-25eb17661c14` and `91cfb54b-22ee-4350-be74-a272830687cd`.

## Setting the template values

We have now all the information to set the template values. This is done with the following query:

_Query_

```
mutation SetTemplates($licenseGuid: UUID!, $projectGuid: UUID!, $projectVersionGuid: UUID!) {
  projectVersion(licenseGuid: $licenseGuid, projectGuid: $projectGuid, projectVersionGuid: $projectVersionGuid) {
    setDescription: setTemplateValue(
      iconGuid: "fbbc9fab-3c08-4b6e-9561-5894ec12a02f",
      templateGuid: "4e1825a0-142b-45d8-8efb-25eb17661c14",
      value: "Some new description"
    ) {
      ...on TemplateTextValue {
        text
      }
    }

    setUrl: setTemplateValue(
      iconGuid: "fbbc9fab-3c08-4b6e-9561-5894ec12a02f",
      templateGuid: "91cfb54b-22ee-4350-be74-a272830687cd",
      value: "http://www.office.com"
    ) {
      ...on TemplateTextValue {
        text
      }
    }
  }
}
```

And the resulting JSON is:

```json
{
  "data": {
    "projectVersion": {
      "setDescription": {
        "text": "Some new description"
      },
      "setUrl": {
        "text": "http://www.office.com"
      }
    }
  }
}
```

And by loading the project in the Designer we see the following:

![picture](../img/word2.png)
